# Aerolab's Coding Challenge

The challenge is creating a catalog view for a loyalty program application.
You must meet the following requirements:
- Each product should have a visible price in points.
- The user should be able to sort products by price, from highest to lowest, and vice-versa.
- The user should be able to see how many points they have in their account.
- There should be a clear way for the user to distinguish those products that they can redeem from those they cannot.
- A “Redeem” button should be available for those products that the user has enough points to claim.
- A “Redeem now” option should appear when the user interacts with a product that they have enough points to claim.
- When the user doesn’t have enough points for a product, they should be able to see how many more points they need to claim it.
- The user should not be able to redeem a product for which they don’t have enough points.
- When the user clicks on the Redeem now button, the system should automatically deduct the item’s price from the users’ points.

In this resolution they were added:

- Changes in the design, making it responsive and adding animations.
- It was made PWA and the possibility of it working offline
- A menu was added with the history of the products that have been exchanged


#### Instalación

*To clone the repository:*
```
  git clone https://gitlab.com/san.boffa/coding-challenge-aerolab.git
```

*Install*
```
  yarn install
```

*Configure .env*

Create .env with these parameters
```
    REACT_APP_API_URL="url"
    REACT_APP_ACCCES_API="TOKEN"
```

*Start*
```
  yarn start
```
```

          You can now view it in the browser.

          Local:            http://localhost:3000/

```

*Build del proyecto*
```
  yarn build
```