var CACHE_NAME = 'offline-first';

var CACHE_FILES = [
  './',
  './icons/coin.svg',
  './aerolab-logo.svg',
  './header-x1.png',
  '../src/App.css',
  '../src/index.js',
]


self.addEventListener('install', event => {

  event.waitUntil(async function () {
    const cache = await caches.open(CACHE_NAME);
    return cache.addAll(CACHE_FILES);
  }());
});


self.addEventListener('activate', event => {

  event.waitUntil(async function () {
    const cachesKeys = await caches.keys();
    const deletePromises = cachesKeys.map((cacheName) => {
      if (cacheName !== CACHE_NAME) {
        return caches.delete(cacheName);
      }
    })

    return await Promise.all(deletePromises);
  }());
});


self.addEventListener('fetch', event => {

  event.respondWith(async function () {
    try {
      const cachedResponse = await caches.match(event.request);

      if (cachedResponse) {
        return cachedResponse;
      }

      const response = await fetch(event.request);
      if (!response) {
        return response;
      }

      const responseClone = response.clone();
      const cache = await caches.open(CACHE_NAME);
      cache.put(event.request, responseClone);

      return response;
    } catch (err) {

    }
  }());
});
