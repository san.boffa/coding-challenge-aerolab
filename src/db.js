import Dexie from 'dexie';

const db = new Dexie('challenge');

db.version(1).stores({ redeem: '++id, &_id, request' });

export default db;