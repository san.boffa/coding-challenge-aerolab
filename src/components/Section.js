import React from "react";
import styled from "styled-components";

const StyledSection = styled.article`
  margin: 0 132px;

  @media (max-width: 900px) {
    margin: 0 70px;
  }

  @media (max-width: 768px) {
    margin: 0 15px;
  }

`;

const Section = ({ children }) => {
  return <StyledSection>{children}</StyledSection>;
};

export default Section;
