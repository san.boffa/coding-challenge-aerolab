import React from "react";
import styled from "styled-components";
import UserInfo from "./UserInfo";

const StyledNav = styled.header`
  display: fixed;
  position: fixed;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 80px;
  top:0;
  z-index: 999;

  background:#FFFFFF;

  @media (max-width: 400px) {
    display: flex;
    justify-content: end;
    flex-direction: column;
    height: 150px;
  }
`;

const StyledLogo = styled.img`
  margin: 22px 42px;
`;

const Nav = (props) => {
  const { className } = props;

  return (
    <StyledNav className={"header " + className}>
      <StyledLogo src="/aerolab-logo.svg" alt="Aerolab" />
      <UserInfo />
    </StyledNav>
  );
};

export default Nav;
