import React from "react";
import styled from "styled-components";

const StyledCardFront = styled.div`
    position: absolute;
    width: 100%;
    height: 100%;

    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
`;

const StyledHeader = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    min-height: 150px;
`;

const StyledImg = styled.img`
    border-radius: 20px;
    min-height: 70px;
`;

const StlyedCardSeparator = styled.hr`
    margin: 10px 24px;
    border-top: 1px solid #D9D9D9;
`;

const StyledDescription = styled.div`
    height: 64px;
    margin: 0 24px;
`;

const StyledCategory = styled.h2`
    margin: 20px 0 0;

    font-size: 16px;
    line-height: 20px;
    letter-spacing: -0.0376471px;
    color: #A3A3A3;
`;

const StyledIProductName = styled.h1`
    margin: 0 0 20px;

    font-size: 18px;
    line-height: 23px;
    letter-spacing: -0.0423529px;
    color: #616161;
`;

const ProductCard = (props) => {
    return (
        <StyledCardFront>
            <StyledHeader>
                <StyledImg src={props.product.img ? props.product.img.url : "/aerolab-logo.svg"} alt={props.product.name} />
            </StyledHeader>
            <StlyedCardSeparator />
            <StyledDescription>
                <StyledCategory>{props.product.category}</StyledCategory>
                <StyledIProductName>{props.product.name}</StyledIProductName>
            </StyledDescription>
        </StyledCardFront>
    );
};

export default ProductCard;

