import React, { useContext } from "react";
import styled from "styled-components";

import { AiFillGift } from "react-icons/ai";

import UserContext from "../contexts/UserContext";
import MenuContext from "../contexts/MenuContext";


const StyledUserInfo = styled.div`
    display: flex;
    margin-right: 42px;

    @media (max-width: 400px) {
       margin:0;
    }
`;

const StyledUserName = styled.div`
    margin: auto;

    font-size: 24px;
    line-height: 48px;
    color: #616161;
`;

const StyledPoints = styled.div`
    display:flex;
    margin: 0 10px;
    padding: 0 12px;
    
    border-radius: 20.5px;

    background-color: #EDEDED;

    font-size: 24px;
    line-height: 48px;
`;

const StyledCoint = styled.img`
    margin-top: 3px;
    margin-left: 5px;   
`;

const StyledMountOfCoint = styled.span`
    margin: auto;
    color: #616161;
`;

const StyledButtonHistory = styled(AiFillGift)`
    margin: auto 0;
    padding: 12px;

    cursor:pointer;
    background-color: #ebebeb;

    border-radius: 25.5px;

    font-size: 31px;
    color: #6c6161;

    transition: background-color 500ms linear;

    &.selected{
        background-color: #0AD4FA;
        color: #fff;
        
        transition: background-color 500ms linear;
    }

`;

const UserInfo = () => {
    const { user } = useContext(UserContext);
    const { openHistory, setOpenHistory } = useContext(MenuContext);

    return (
        <StyledUserInfo>
            <StyledUserName>{user.name}</StyledUserName>
            <StyledPoints>
                <StyledMountOfCoint>{user.points}</StyledMountOfCoint>
                <StyledCoint src="/icons/coin.svg" alt="Coint" />
            </StyledPoints>
            <StyledButtonHistory className={openHistory ? "selected" : ""} onClick={() => setOpenHistory(prev => !prev)} />
        </StyledUserInfo>
    );
};

export default UserInfo;
