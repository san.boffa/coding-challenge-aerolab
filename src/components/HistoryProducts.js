import React, { useContext } from "react";
import styled from "styled-components";

import UserContext from "../contexts/UserContext";
import MenuContext from "../contexts/MenuContext"

import DescriptionProductCard from "./DescriptionProductCard"

const StyledHistorySection = styled.div`
    display: flex;
    position: fixed;
    flex-direction: column;
    width: 350px;
    height: calc(100vh - 80px);
    margin-top:80px;
    right: -500px;
    z-index: 999;

    overflow-y: scroll;
    overflow-x: hidden;
    
    background-color: #dadada;

    transition:right 1s;

    &.apperar{
       right: 0; 
    }

    @media (max-width: 400px) {
        width: 100%;
        height: calc(100vh - 150px);
        margin-top:150px;
    }
   
`;

const StyledTitleHistory = styled.h2`
    font-size: 24px;
    text-align: center;
    color: #616161;
`;

const StyledGiftCard = styled.div`
    display: inline-table;
    position: relative;
    width: 80%;
    height: 276px;
    margin: 10px auto;
    
    background-color: #FFFFFF;

    box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.1);
    border-radius: 20.5px;
`;

const HistoryProducts = () => {
    const { user } = useContext(UserContext);
    const { openHistory } = useContext(MenuContext);

    return (
        <StyledHistorySection
            className={openHistory ? "apperar" : ""}
        >
            <StyledTitleHistory>History Products Redeem</StyledTitleHistory>
            {user.redeemHistory &&
                user.redeemHistory.slice(0).reverse().map((e, i) => {
                    return (
                        <StyledGiftCard key={e.name + "-" + i}>
                            <DescriptionProductCard product={e} />
                        </StyledGiftCard>
                    )
                })
            }
        </StyledHistorySection>
    );
};

export default HistoryProducts;
