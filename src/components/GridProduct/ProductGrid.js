import React, { useContext, useEffect, useCallback } from "react";
import styled from "styled-components";

import ProductCard from "./ProductCard";

import GridContext from "../../contexts/GridContext";

const StyledGrid = styled.ul`
    display: grid;
    margin: 0;
    padding: 0;
    justify-content: center;
    align-content: center;
    
    grid-template-columns: repeat(auto-fill, 276px);
    grid-gap: 24px;
`;

const ProductGrid = (props) => {
    const {
        products,
        setProducts,
        setCountProducts,
        page,
        selectFilter,
        paginateProducts,
        setPaginateProducts
    } = useContext(GridContext);
    const { filters } = props;

    const filterPaginateProducts = useCallback((allProducts) => {
        let paginate = [];
        var i, j, temparray, chunk = 16;
        const filterPoducts =
            selectFilter !== "" &&
                filters[selectFilter].filter ?
                allProducts.slice().sort(filters[selectFilter].filter) :
                allProducts;

        for (i = 0, j = filterPoducts.length; i < j; i += chunk) {
            temparray = filterPoducts.slice(i, i + chunk);
            paginate.push(temparray)
        }

        return paginate
    }, [selectFilter, filters])


    useEffect(() => {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + process.env.REACT_APP_ACCCES_API);
        myHeaders.append("Content-Type", "application/json");

        var requestOptions = {
            method: "GET",
            headers: myHeaders
        };

        const fetchProducts = async () => {
            const r = await fetch(process.env.REACT_APP_API_URL + "/products", requestOptions)
            const response = await r.json();

            setCountProducts(response.length)
            setProducts(response);

            const respdiv = await filterPaginateProducts(response)
            setPaginateProducts(respdiv);
        }

        fetchProducts();

    }, [filterPaginateProducts, setCountProducts, setProducts, setPaginateProducts,]);

    useEffect(() => {
        const respDiv = filterPaginateProducts(products);
        setPaginateProducts(respDiv);
    }, [selectFilter, products, filterPaginateProducts, setPaginateProducts]);

    return (
        <StyledGrid>
            {paginateProducts[page - 1] && paginateProducts[page - 1].map((product, i) =>
                < ProductCard product={product} key={i} />
            )}
        </StyledGrid>
    );
}

export default ProductGrid;
