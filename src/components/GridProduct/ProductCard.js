import React, { useState, useContext } from "react";
import styled from "styled-components";
import { isMobile } from 'react-device-detect';
import { AiOutlineCheckCircle } from "react-icons/ai";

import db from "../../db"

import UserContext from "../../contexts/UserContext";

import DescriptionProductCard from "../DescriptionProductCard"

const StyledWrappedContainer = styled.li`
    display: inline-grid;
`;

const StyledContainer = styled.div`
    position: relative;
    width: 276px;
    height: 276px;

    background-color: #FFFFFF;
    cursor: pointer;
    
    box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.1);
    border-radius: 20.5px;

    transition: transform 1s;
    transform-style: preserve-3d;

    &.is-flipped{
        transform: rotateY(180deg);
    }
`;

const StyledIProductName = styled.h1`
    margin: 0 0 20px;
    
    font-size: 18px;
    line-height: 23px;
    letter-spacing: -0.0423529px;
    
    color: #616161;
`;

const StyledCardSuccess = styled.div`
    display: flex;
    align-content: center;
    flex-direction: column;
    justify-content: center;
    text-align: center;
    color: #616161;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    transform: rotateY(180deg);
`;

const StyledSuccessIcon = styled(AiOutlineCheckCircle)`
    color: #87d185;
    font-size: 36px;
    margin: 51px auto 0;
`;

const StyledCardBack = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    position: absolute;
    
    width: 100%;
    height: 100%;
    
    box-shadow: 2px 2px 4px rgba(0, 0, 0, 0.1);
    border-radius: 20.5px;
    
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    transform: rotateY(180deg);
    background-color:#0AD4FA;

    &.disabled {
        background-color: #a2a2a2;
    }
`;

const StyledDescriptionBack = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

const StyledButtonBack = styled.button`
    display: block;
    margin-top: 20px;
    padding: 0 64px;
    
    cursor: pointer;
    
    border-radius: 20.5px;
    border: none;
    
    font-size: 1rem;
    line-height: 40px;
    color: #616161;
    
    background-color: #FFFFFF;
`;

const StyledCoints = styled.span`
    font-size: 36px;
    font-weight: bold;
    line-height: 45px;
    text-align: center;
    letter-spacing: -0.0847059px;
    color: #FFFFFF;
`;

const StyledCoint = styled.img`
    margin-left: 5px;   
`;

const StlyedTextDisabledCard = styled.span`
    font-size: 24px;
    line-height: 45px;
    font-weight: bold;
    text-align: center;
    letter-spacing: -0.0847059px;
    color: #FFFFFF;
`;



const ProductCard = (props) => {
    const [flip, setFlip] = useState(false);
    const [success, setSuccess] = useState(false);
    const { user, setUser, isOffline } = useContext(UserContext);
    const { product } = props;

    const handelClickRedeeem = (e, product) => {
        e.stopPropagation()
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + process.env.REACT_APP_ACCCES_API);
        myHeaders.append("Content-Type", "application/json");

        var data = {
            "productId": product._id
        }

        var requestOptions = {
            method: "POST",
            headers: myHeaders,
            body: JSON.stringify(data),
            redirect: "follow",
        };

        const actionsRedeem = (product, userRe) => {
            userRe.points = userRe.points - product.cost;
            userRe.redeemHistory = [...userRe.redeemHistory, product]
            setUser({ ...userRe })
            setSuccess(true)
            setTimeout(function () { setSuccess(false); setFlip(false) }, 3000);
        }

        if (isOffline) {
            db.table("redeem").add({ request: requestOptions, cost: product.cost });
            actionsRedeem(product, user);
        } else {
            fetch(process.env.REACT_APP_API_URL + "/redeem", requestOptions)
                .then((r) => r.json())
                .then((response) => {
                    actionsRedeem(product, user);
                })
                .catch((error) => console.log(error));
        }
    }

    return (
        <StyledWrappedContainer
            onMouseEnter={() => !isMobile && setFlip(true)}
            onMouseLeave={() => !isMobile && setFlip(false)}
            onClick={() => { isMobile && setFlip(prev => !prev); console.log("ENTRA") }}
        >
            <StyledContainer
                className={flip ? "is-flipped" : ""}
            >

                <DescriptionProductCard product={product} />
                {success ?
                    <StyledCardSuccess>
                        <StyledSuccessIcon />
                        <h1>Thank you!</h1>
                        <span>Your product was successfully redeemed.</span>
                    </StyledCardSuccess> :
                    product.cost > user.points ?
                        <StyledCardBack className="disabled">
                            <StyledIProductName>{product.name}</StyledIProductName>
                            <StlyedTextDisabledCard>You Need:</StlyedTextDisabledCard>
                            <StyledDescriptionBack>
                                <StyledCoints>{product.cost}</StyledCoints>
                                < StyledCoint src="/icons/coin.svg" alt="Coint" />
                            </StyledDescriptionBack>
                        </StyledCardBack>
                        : <StyledCardBack>
                            <StyledIProductName>{product.name}</StyledIProductName>
                            <StyledDescriptionBack>
                                <StyledCoints>{product.cost}</StyledCoints>
                                <StyledCoint src="/icons/coin.svg" alt="Coint" />
                            </StyledDescriptionBack>
                            <StyledButtonBack onClick={(e) => handelClickRedeeem(e, product)}>
                                Redeem now
                            </StyledButtonBack>
                        </StyledCardBack>
                }
            </StyledContainer>
        </StyledWrappedContainer>
    );
};

export default ProductCard;

