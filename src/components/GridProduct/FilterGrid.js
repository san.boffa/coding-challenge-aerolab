import React, { useContext } from "react";
import styled from "styled-components";

import GridContext from "../../contexts/GridContext";

import {
    AiFillCloseCircle,
    AiOutlineRightCircle,
    AiOutlineLeftCircle
} from "react-icons/ai";

const StyledFilterGrid = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;

    height: 120px;
    width: 100%;

    @media (max-width: 768px) {
        height: 170px;
        flex-direction: column;
    }
`;

const StyledSecondaryText = styled.span`
    margin-right: 10px;
    align-self: center;
    
    font-size: 1rem;
    line-height: 48px;
    letter-spacing: -0.15px;
    color: #A3A3A3;

    @media (max-width: 1200px) {
        align-self: center;
        margin-right: 10px;
    }
`;

const StyledButtonGroup = styled.ul`
    display: flex;
    padding: 0;

    @media (max-width: 1200px) {
        justify-content: center;
    }
`;

const StyledFilters = styled.div`
    display: flex;

    @media (max-width: 1200px) {
        align-content: center;
        justify-content: center;
    }

    @media (max-width: 768px) {
        margin: 0 10px;
    }

    @media (max-width: 420px) {
        flex-direction: column;
    }
`;

const StyledFilterSeparator = styled.hr`
    width: 1px;
    height: 25px;
    margin: 0px 7px;

    border: none;
    border-left: 1px solid #fff;
`;

const StyledFilterOptions = styled.li`
    display: flex;
    align-items: center;
    padding: 0.7rem 1rem;
    margin: 0 0.3rem;
    background-color: #EDEDED;
    
    cursor: pointer;
    border-radius: 20.5px;
    
    font-size: 1.1rem;
    line-height: 1.1rem;
    color: #A3A3A3;
    
    transition: background-color 200ms linear;

    &.selected{
        background-color: #0AD4FA;
        color: #fff;
        
        transition: background-color 800ms linear;
    }
    
`;

const StyledPaginate = styled.div`
    display: flex;

    @media (max-width: 1200px) {
        align-content: center;
        justify-content: flex-end;
    }
`;

const StyledCountProduct = styled.span`
    margin: auto 10px;

    font-style: normal;
    font-weight: normal;
    font-size: 1.1rem;
    line-height: 1.1rem;
    color: #616161;

    @media (max-width: 1200px) {
        align-self: end
    }
`;

const StyledCountSeparator = styled.hr`
    width: 1px;
    margin: 0 24px;
    
    border: none;
    border-left: 1px solid hsla(200, 10%, 50%,100);

    @media (max-width: 1200px) {
       display:none;
    }
`;

const StyledArrow = styled.a`
    display: flex;
    align-items: center;
    margin: 0 10px 0 0;
    
    background-color: transparent;
    border: none;
    
    font-size: 1.8em;
    color: #A3A3A3;
`;

const FilterGrid = (props) => {
    const {
        selectFilter,
        setSelectFilter,
        page,
        setPage,
        countProducts
    } = useContext(GridContext);

    return (
        <StyledFilterGrid>
            <StyledFilters>
                <StyledSecondaryText>Sort by:</StyledSecondaryText>
                <StyledButtonGroup>
                    {props.filters.map((e, i) =>
                        <StyledFilterOptions
                            key={e.name}
                            onClick={() =>
                                selectFilter === i ?
                                    setSelectFilter("") :
                                    setSelectFilter(i)
                            }
                            className={
                                selectFilter === i ?
                                    "selected" :
                                    ""
                            }>
                            {selectFilter === i ?
                                <>
                                    {e.name}
                                    <StyledFilterSeparator />
                                    <AiFillCloseCircle />
                                </> :
                                e.name
                            }
                        </StyledFilterOptions>
                    )}
                </StyledButtonGroup>
            </StyledFilters>
            <StyledPaginate>
                <StyledCountProduct>
                    {page * 16 < countProducts ?
                        page * 16 :
                        countProducts
                    }
                     of {countProducts} products
                    </StyledCountProduct>
                <StyledCountSeparator />
                <StyledArrow onClick={() => setPage(prev => prev - 1 > 0 ? (prev - 1) : prev)}>
                    <AiOutlineLeftCircle />
                </StyledArrow>
                <StyledArrow onClick={() => setPage(prev => countProducts - (prev * 16) > 0 ? (prev + 1) : prev)}>
                    <AiOutlineRightCircle />
                </StyledArrow>
            </StyledPaginate>
        </StyledFilterGrid>
    );
}

export default FilterGrid;
