import React from "react";
import styled, { css } from "styled-components";

const StyledHeader = styled.div`
    display: flex;
    align-items: flex-end;
    width: 100%;
    height: 22vw;

    ${props => props.backgroundImage &&
        css`background-image: url('${props.backgroundImage}');`

    }
    background-position: center;
    background-repeat: no-repeat; 
    background-size: cover; 

    @media (max-width: 400px) {
        height: 15vh;
    }
`;

const StyledHeaderTitle = styled.h1`
    margin-bottom: 5%;
    margin-left: 10%;

    color: #FFFFFF;

    font-style: normal;
    font-weight: bold;
    font-size: 5vw;

    @media (max-width: 768px) {
        font-size: 5vh;
    }
`;

const Header = (props) => {
    return (
        <StyledHeader {...props} >
            <StyledHeaderTitle >{props.title}</StyledHeaderTitle>
        </StyledHeader>
    );
};

export default Header;
