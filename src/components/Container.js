import React from "react";
import styled from "styled-components";

const StyledContainer = styled.div`
  width: 100%;
  padding-top: 80px;
  background-color: #F2F2F2;
  
  @media (max-width: 400px) {
    padding-top: 150px;
 }
`;

const Container = (props) => {
  return <StyledContainer {...props}>{props.children}</StyledContainer>;
};

export default Container;
