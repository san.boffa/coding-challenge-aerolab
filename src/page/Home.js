import React from "react";

import { GridProvider } from "../contexts/GridContext";

import Container from "../components/Container";
import Section from "../components/Section"
import Header from "../components/Header"
import FilterGrid from "../components/GridProduct/FilterGrid"
import ProductGrid from "../components/GridProduct/ProductGrid"

const filters = [
    {
        name: "Lowest price",
        filter: (a, b) => { return a.cost - b.cost }
    },
    {
        name: "Highest price",
        filter: (a, b) => {
            if (a.cost < b.cost) {
                return 1;
            }
            if (a.cost > b.cost) {
                return -1;
            }
            return 0;
        }
    }
];

const Home = () => {
    return (
        <Container>
            <Header title="Electronics" backgroundImage="/header-x1.png" />
            <Section>
                <GridProvider>
                    <FilterGrid filters={filters} />
                    <hr />
                    <ProductGrid filters={filters} />
                </GridProvider>
            </Section>
        </Container>
    );
}

export default Home;
