import React, { createContext, useState } from "react";

const MenuContext = createContext();

const MenuProvider = ({ children }) => {
    const [openHistory, setOpenHistory] = useState(false);

    return (
        <MenuContext.Provider
            value={{
                openHistory,
                setOpenHistory
            }}
        >
            {children}
        </MenuContext.Provider>
    );
};

export { MenuProvider };
export default MenuContext;
