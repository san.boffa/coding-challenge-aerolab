import React, { createContext, useEffect, useState } from "react";

import db from "../db"

const UserContext = createContext();

const UserProvider = ({ children }) => {
  const [user, setUser] = useState({});
  const [isOffline, setIsOffline] = useState(false);

  var myHeaders = new Headers();
  myHeaders.append("Authorization", "Bearer " + process.env.REACT_APP_ACCCES_API);
  myHeaders.append("Content-Type", "application/json");

  var requestOptions = {
    method: "GET",
    headers: myHeaders,
    redirect: "follow",
  };

  useEffect(() => {
    fetch(process.env.REACT_APP_API_URL + "/user/me", requestOptions)
      .then((r) => r.json())
      .then(async (response) => {
        const redeem = await db.table("redeem").toArray();

        if (redeem.length > 0) {
          var userTemp = response;
          const spent = redeem.reduce((accumulator, currentValue) => accumulator + currentValue.cost, 0)
          userTemp.points = response.points - spent;
          setUser({ ...userTemp })
        } else {
          setUser(response);
        }

      })
      .catch((error) => console.log(error));
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    window.addEventListener("online", () => {
      console.log("entra")
      const fetchRedeem = async () => {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", "Bearer " + process.env.REACT_APP_ACCCES_API);
        myHeaders.append("Content-Type", "application/json");

        const redeem = await db.table("redeem").toArray();
        if (redeem.length > 0) {
          redeem.map((e) => {
            e.request.headers = myHeaders;
            fetch(process.env.REACT_APP_API_URL + "/redeem", e.request);
            db.table("redeem").where({ id: e.id }).delete();
            return "";
          })
        }
      }

      fetchRedeem();
      setIsOffline(false);

    });
    window.addEventListener("offline", () => {
      setIsOffline(true);
    });
  }, []);

  return (
    <UserContext.Provider
      value={{
        user,
        setUser,
        isOffline,
        setIsOffline
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export { UserProvider };
export default UserContext;
