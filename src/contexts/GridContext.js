import React, { createContext, useState } from "react";

const GridContext = createContext();

const GridProvider = ({ children }) => {
    const [page, setPage] = useState(1);
    const [selectFilter, setSelectFilter] = useState("");
    const [products, setProducts] = useState([[]]);
    const [paginateProducts, setPaginateProducts] = useState([[]]);
    const [countProducts, setCountProducts] = useState(0);

    return (
        <GridContext.Provider
            value={{
                page,
                setPage,
                selectFilter,
                setSelectFilter,
                products,
                setProducts,
                countProducts,
                setCountProducts,
                paginateProducts,
                setPaginateProducts
            }}
        >
            {children}
        </GridContext.Provider>
    );
};

export { GridProvider };
export default GridContext;
