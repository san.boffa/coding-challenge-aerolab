import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { ThemeProvider } from "styled-components";

import { UserProvider } from "./contexts/UserContext";
import { MenuProvider } from "./contexts/MenuContext";

import Home from "./page/Home";
import Nav from "./components/Nav"
import HistoryProducts from "./components/HistoryProducts"

const theme = {

};

const App = (props) => {
  return (
    <Router>
      <ThemeProvider theme={theme}>
        <UserProvider>
          <MenuProvider>
            <Nav />
            <HistoryProducts />
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
            </Switch>
          </MenuProvider>
        </UserProvider>
      </ThemeProvider>
    </Router>
  );
};

export default App;
